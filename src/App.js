import './App.css';
import React from 'react';
import Home from '.././src/component/page/Home/Home';
import Login from './component/page/Login/Login';
import Write from './component/page/Write/Write';
import Single from './component/Single/Single';
import Topbar from './component/Topbar';
import Register from './component/page/Register/Register';
import { Route, Routes } from 'react-router-dom';
import Setting from './component/page/Setting/Setting';



function App() {
  const currentUser = true;
  return (
    <div className='currentUser'>
      <Topbar/>

<Routes>
<Route exact path="/"
      element={<Home />}/>
        <Route path="/posts"
         element= {<Home />}/>
        <Route path="/register"
          element={currentUser ? <Home /> : <Register />}/>
        <Route path="/login" element={currentUser ? <Home /> : <Login />}/>
        <Route path="/post/:id"
         element= {<Single />}/>
        <Route path="/write" element={currentUser ? <Write /> : <Login />}/>
        <Route path="/setting"
         element= {currentUser ? <Setting /> : <Login />} />

</Routes>
</div>
);
}

export default App;
