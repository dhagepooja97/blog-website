import Siderbar from '../../Header/Siderbar'
import '../Single/Single.css'
import SinglePost from '../SinglePost/SinglePost'


export default function Single() {
  return (
    <div className='single'>
                <SinglePost/>

        <Siderbar/>
    </div>
  )
}
