import React from 'react'
import Header from '../../../Header/Header';
import './Home.css'
import Post from '../../posts/Post';
import Siderbar from'../../../Header/Siderbar';


function Home() {
  const location = (0);
  console.log(location);
  return (
    <>
      <Header />
      <div className="home">
        <Post />
        <Siderbar />
      </div>
    </>
  )
}
export default Home;