import React from 'react'
import '../Header/Siderbar.css'

 function Siderbar() {
  return (
  
    <div className='sidebar'>
<div className='SiderbarItem'>
<span className='siderbarTitle'>About Me</span>
<img
src='https://images.unsplash.com/photo-1679879153947-7f2f9d3d4225?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=435&q=80://themegoods-cdn-pzbycso8wng.stackpathdns.com/grandblog/demo/wp-content/uploads/2015/11/aboutme.jpg'         
 alt=""
        />
 <p>          Laboris sunt aute cupidatat velit magna velit ullamco dolore mollit
          amet ex esse.Sunt eu ut nostrud id quis proident.
</p>

</div>
<div className='SiderbarItem'>
<span className='siderbarTitle'>Categories</span>
<ul className='siderbarList'>
  <li className='siderbarListItem'>Life</li>
  <li className='siderbarListItem'>Sport</li>
  <li className='siderbarListItem'>Music</li>
  <li className='siderbarListItem'>Style</li>
  <li className='siderbarListItem'>Tech</li>
  <li className='siderbarListItem'>Cinema</li>

</ul>
<div className='SiderbarItem'>
  <span className='siderbarTitle'>Follow Us</span>
<div className='siderbarSocial'>
<i className="sidebarIcon fab fa-facebook-square"></i>
          <i className="sidebarIcon fab fa-instagram-square"></i>
          <i className="sidebarIcon fab fa-pinterest-square"></i>
          <i className="sidebarIcon fab fa-twitter-square"></i>

</div>


</div>
</div>
    </div>
  
  )
}
export default Siderbar;